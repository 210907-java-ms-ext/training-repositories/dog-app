import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Dog } from '../models/Dog';
import { DogDataService } from '../services/dog-data.service';


@Component({
  selector: 'app-dog-catalog',
  templateUrl: './dog-catalog.component.html',
  styleUrls: ['./dog-catalog.component.css']
})
export class DogCatalogComponent implements OnInit {

  dogs: Dog[] = [];
  loading: boolean = true;

  constructor(private dogData: DogDataService) { }

  ngOnInit(): void {
    this.dogData.getAllDogs().subscribe(
      result=>{
        console.log(result)
        this.dogs = result;
        this.loading = false;
      }
    )
  }

}
