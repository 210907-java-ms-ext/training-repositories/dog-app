import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DogCatalogComponent } from './dog-catalog.component';

describe('DogCatalogComponent', () => {
  let component: DogCatalogComponent;
  let fixture: ComponentFixture<DogCatalogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DogCatalogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DogCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
