export class Breed {

    id:number;
    name:string;
    lifespan:string;
    size:string;

    constructor(id: number, name: string, lifespan: string, size: string ){
        this.id = id;
        this.name = name;
        this.lifespan = lifespan;
        this.size = size;
    }
}