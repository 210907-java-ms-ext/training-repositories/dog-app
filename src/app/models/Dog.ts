import { Breed } from "./Breed";

export class Dog {
    
    id:number;
    name:string;
    breed:Breed;

    constructor(id: number, name: string, breed: Breed){
        this.id = id;
        this.name = name;
        this.breed = breed;
    }
}