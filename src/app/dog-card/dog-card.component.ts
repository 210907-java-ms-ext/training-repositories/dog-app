import { Component, Input, OnInit } from '@angular/core';
import { Dog } from '../models/Dog';
import { DogImgService } from '../services/dog-img.service';

@Component({
  selector: 'app-dog-card',
  templateUrl: './dog-card.component.html',
  styleUrls: ['./dog-card.component.css']
})
export class DogCardComponent implements OnInit {

  @Input()
  dogData!: Dog;

  imgUrl: string = "https://flyclipart.com/thumb2/adopt-a-dog-harry-pug-dogs-trust-200069.png";

  constructor(private dogImgService:DogImgService) { }

  ngOnInit(): void {
    let breed = this.dogData.breed;
    if(breed){
      this.dogImgService.getImageUrl(breed.name.toLowerCase()).subscribe(
        result => { 
          if (result.status == "success"){
            this.imgUrl = result.message;
          } 
        }
      )
    }
    
  }

}
