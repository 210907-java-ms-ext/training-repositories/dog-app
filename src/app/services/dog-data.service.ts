import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Breed } from '../models/Breed';
import { Dog } from '../models/Dog';
import { ImageResponse } from '../models/ImageResponse';

@Injectable({
  providedIn: 'root'
})
export class DogDataService {

  private DATA_URL: string = "http://localhost:8082/hibernate-spring-mvc/dogs";

  constructor(private http: HttpClient) { }

  getAllDogs(): Observable<Dog[]>{
    return this.http.get<Dog[]>(this.DATA_URL,{headers: {"Authorization": "token"}});
  }
}
