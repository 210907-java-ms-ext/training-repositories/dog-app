import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ImageResponse } from '../models/ImageResponse';

@Injectable({
  providedIn: 'root'
})
export class DogImgService {

  constructor(private http: HttpClient) { }

  
  getImageUrl(breed:string): Observable<ImageResponse>{
    return this.http.get<ImageResponse>(`https://dog.ceo/api/breed/${breed}/images/random`);
  }
}
