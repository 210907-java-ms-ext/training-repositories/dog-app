import { TestBed } from '@angular/core/testing';

import { DogImgService } from './dog-img.service';

describe('DogImgService', () => {
  let service: DogImgService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DogImgService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
