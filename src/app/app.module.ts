import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { DogCatalogComponent } from './dog-catalog/dog-catalog.component';
import { DogCardComponent } from './dog-card/dog-card.component';
import { DogDataService } from './services/dog-data.service';

@NgModule({
  declarations: [
    AppComponent,
    DogCatalogComponent,
    DogCardComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    DogDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
